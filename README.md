## ⚠️ Warning!!
This version is deprecated and it will soon be removed from the repository. Use the [Dev](https://gitlab.com/kreato/pogtap/-/tree/dev) branch whenever possible.
DO NOT make a issue if you have issues with this version.
# Pogtap
> Pogtap is a paste based off Weave

## Requirements

1. Visual studio 2019 
2. DirectX SDK 

## 🚀 Getting Started

```
git clone https://gitlab.com/kreato/pogtap.git
```

## 🤝 Contributing

1. [Fork the repository](https://gitlab.com/kreato/pogtap/-/forks/new)
2. Clone your fork: `git clone https://gitlab.com/your-username/pogtap.git`
3. Create your feature branch: `git checkout -b my-new-feature`
4. Commit your changes: `git commit -am 'Add some feature'`
5. Push to the branch: `git push origin my-new-feature`
6. Submit a pull request

## 📝 "Developers"

Itsme
Kreato
Sorry
