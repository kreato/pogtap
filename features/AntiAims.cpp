#include "../hooks/Hooks.h"
#include "AntiAims.h"
#include "Ragebot.h"

static float last_angle;

bool CanDT() {
	int idx = csgo->weapon->GetItemDefinitionIndex();
	return csgo->local->isAlive()
		&& csgo->weapon->DTable()
		&& csgo->client_state->iChokedCommands <= 1
		&& idx != WEAPON_REVOLVER
		&& idx != WEAPON_ZEUSX27
		&& vars.ragebot.double_tap->active
		&& !csgo->fake_duck;
}



bool Airstuck() {
	if (vars.ragebot.air_stuck->active)
	{
		csgo->cmd->command_number = INT_MAX;
		csgo->cmd->tick_count = INT_MAX;
	}
	return false;
}



/*bool CanTP() {
	return csgo->local->isAlive()
		&& csgo->client_state->iChokedCommands <= 1
		&& vars.ragebot.teleport->active
		&& !csgo->fake_duck;
}*/

bool CanHS() {
	int idx = csgo->weapon->GetItemDefinitionIndex();
	return csgo->local->isAlive() && !csgo->weapon->IsMiscWeapon() && csgo->client_state->iChokedCommands
		&& vars.ragebot.hideshots->active && !csgo->fake_duck;
}

void CMAntiAim::Fakelag(bool& send_packet)
{
	if (!vars.antiaim.enable)
		return;

	bool dt = CanDT();
	bool hs = CanHS();
//	bool tp = CanTP();

	bool exp = dt || hs; // || tp;

	if (csgo->fake_duck && csgo->local->GetFlags() & FL_ONGROUND && !(csgo->cmd->buttons & IN_JUMP))
	{
		if (csgo->local->GetFlags() & FL_ONGROUND)
			return;
	}

	if (CanHS()
		|| interfaces.engine->IsVoiceRecording()) {
		csgo->max_fakelag_choke = 2;
		return;
	}

	if (dt && did_shot)
		return;

	if ((csgo->cmd->buttons & IN_ATTACK) && !vars.antiaim.fakelag_onshot && !vars.antiaim.ddos_onshot) {
		send_packet = true;
		csgo->max_fakelag_choke = exp ? 2 : 2;
		return;
	}

	if (exp)
	{
		send_packet = exp ? csgo->client_state->iChokedCommands >= 1 : csgo->client_state->iChokedCommands >= 1;

		csgo->max_fakelag_choke = 2;
		return;
	}

	if (vars.antiaim.fakelag_flags & 1 && csgo->local->GetFlags() & FL_ONGROUND && csgo->local->GetVelocity().Length2D() < 4.f)
		csgo->max_fakelag_choke = vars.antiaim.max_fakelag;
	else if (!(vars.antiaim.fakelag_flags & 1) && csgo->local->GetFlags() & FL_ONGROUND && csgo->local->GetVelocity().Length2D() < 4.f)
		csgo->max_fakelag_choke = 2;

	if (vars.antiaim.fakelag_flags & 2 && csgo->local->GetFlags() & FL_ONGROUND && csgo->local->GetVelocity().Length2D() > 4.f)
		csgo->max_fakelag_choke = vars.antiaim.max_fakelag;
	else if (!(vars.antiaim.fakelag_flags & 2) && csgo->local->GetFlags() & FL_ONGROUND && csgo->local->GetVelocity().Length2D() > 4.f)
		csgo->max_fakelag_choke = 2;

	if (vars.antiaim.fakelag_flags & 4 && !(csgo->local->GetFlags() & FL_ONGROUND))
		csgo->max_fakelag_choke = vars.antiaim.max_fakelag;
	else if (!(vars.antiaim.fakelag_flags & 4) && !(csgo->local->GetFlags() & FL_ONGROUND))
		csgo->max_fakelag_choke = 2;


	auto animstate = csgo->local->GetPlayerAnimState();
	if (!animstate)
		return;

	int tick_to_choke = 2;

	static Vector oldOrigin;

	if (!(csgo->local->GetFlags() & FL_ONGROUND))
	{
		csgo->canDrawLC = true;
	}
	else {
		csgo->canDrawLC = false;
		csgo->canBreakLC = false;
	}

	if (vars.antiaim.fakelag < 2)
		tick_to_choke = 2;
	else
	{
		switch (vars.antiaim.fakelag)
		{
		case 0:
			tick_to_choke = 2;
			break;
		case 1:
			tick_to_choke = vars.antiaim.fakelagfactor;
			break;
		case 2:
		{
			int factor = vars.antiaim.fakelagvariance;
			if (factor == 0)
				factor = 15;
			else if (factor > 100)
				factor = 100;

			if (csgo->cmd->command_number % factor < vars.antiaim.fakelagfactor)
				tick_to_choke = min(vars.antiaim.fakelagfactor, csgo->max_fakelag_choke);
			else
				tick_to_choke = 2;
		}
		break;
		case 3:
		{
			tick_to_choke = Math::RandomFloat(vars.antiaim.minimum_fakelag, vars.antiaim.maximum_fakelag);
		} break;
		case 4:
		{
			int factor = vars.antiaim.fakelagvariance;
			if (factor == 0)
				factor = 15;
			else if (factor > 100)
				factor = 100;

			if (csgo->cmd->command_number % factor < vars.antiaim.maximum_fakelag)
				tick_to_choke = Math::RandomFloat(vars.antiaim.minimum_fakelag, vars.antiaim.maximum_fakelag);
			else
				tick_to_choke = 2;
		} break;
		case 5:
		{
			int factor = vars.antiaim.fakelagvariance;
			if (factor == 0)
				factor = 15;
			else if (factor > 100)
				factor = 100;

			if (csgo->cmd->command_number % factor < vars.antiaim.fakelagfactor)
				tick_to_choke = vars.antiaim.fakelagfactor;
			else
				tick_to_choke = vars.antiaim.fakelag_switch_factor;
		} break;
		case 6:
		{
			tick_to_choke = vars.antiaim.fakelagfactor;

			int factor = vars.antiaim.fakelagvariance;
			if (factor == 0)
				factor = 45;
			else if (factor > 100)
				factor = 100;

			if (csgo->cmd->command_number % factor < 75)
				tick_to_choke = min(vars.antiaim.fakelagfactor + 4, csgo->max_fakelag_choke + 4);
			else
				tick_to_choke = vars.antiaim.fakelagfactor - 2;
		} break;
		}
	}

	if (tick_to_choke < 2)
		tick_to_choke = 2;

	if ((csgo->cmd->buttons & IN_ATTACK) && vars.antiaim.ddos_onshot)
	{
		send_packet = true;
		csgo->max_fakelag_choke = exp ? 2 : 25;
		tick_to_choke = 25;
	}

	if (tick_to_choke > csgo->max_fakelag_choke)
		tick_to_choke = csgo->max_fakelag_choke;

	send_packet = csgo->client_state->iChokedCommands >= tick_to_choke;

	static Vector sent_origin = Vector();

	if (csgo->canDrawLC) {
		if (send_packet)
			sent_origin = csgo->local->GetAbsOrigin();

		if ((sent_origin - oldOrigin).LengthSqr() > 4096.f) {
			csgo->canBreakLC = true;
		}
		else
			csgo->canBreakLC = false;

		if (send_packet)
			oldOrigin = csgo->local->GetAbsOrigin();
	}
}
void CMAntiAim::Pitch()
{
	switch (vars.antiaim.pitch)
	{
	case 0:
		break;
	case 1:
		csgo->cmd->viewangles.x = 89;
		break;
	case 2:
		csgo->cmd->viewangles.x = -89;
		break;
	case 3:
		csgo->cmd->viewangles.x = 89.9999999999999999999999999999999; //premium
		break;
	case 4:
		csgo->cmd->viewangles.x = 540;
		break;
	case 5:
		csgo->cmd->viewangles.x = -540;
		break;
	case 6:
		csgo->cmd->viewangles.x = 1080;
		csgo->cmd->viewangles.z = 1080;
		break;
	case 7:
		csgo->cmd->viewangles.x = 0;
		break;
	case 8:
		csgo->cmd->viewangles.x = vars.antiaim.custom_pitch;
		break;
	}
}

void CMAntiAim::Sidemove() {
	if (!csgo->should_sidemove)
		return;

	static bool sw = false;

	float sideAmount = 2 * ((csgo->cmd->buttons & IN_DUCK || csgo->cmd->buttons & IN_WALK) ? 3.f : 0.505f);
	if (csgo->local->GetVelocity().Length2D() <= 0.f || std::fabs(csgo->local->GetVelocity().z) <= 100.f)
		csgo->cmd->forwardmove += sw ? sideAmount : -sideAmount;

	sw = !sw;
}

float CMAntiAim::corrected_tickbase()
{
	CUserCmd* last_ucmd = nullptr;
	int corrected_tickbase = 0;

	corrected_tickbase = (!last_ucmd || last_ucmd->hasbeenpredicted) ? (float)csgo->local->GetTickBase() : corrected_tickbase++;
	last_ucmd = csgo->cmd;
	float corrected_curtime = corrected_tickbase * interfaces.global_vars->interval_per_tick;
	return corrected_curtime;

};

bool update()
{
	float server_time = TICKS_TO_TIME(csgo->local->GetTickBase());
	static float next_break = 0.f;

	auto animstate = csgo->local->GetPlayerAnimState();
	if (!animstate)
		return false;

	if (animstate->m_velocity > 0.1f || fabs(animstate->flUpVelocity) > 100.f)
		next_break = server_time + 0.22f;

	if (next_break < server_time)
	{
		next_break = server_time + 0.22f;
		return true;
	}

	return false;
}

bool fake_head()
{
	float server_time = TICKS_TO_TIME(csgo->local->GetTickBase());
	static float next_break = 0.f;

	auto animstate = csgo->local->GetPlayerAnimState();
	if (!animstate)
		return false;

	if (animstate->m_velocity > 6.0f || fabs(animstate->flUpVelocity) > 100.f)
		next_break = server_time + 1.1f;

	if (next_break < server_time)
	{
		next_break = server_time + 1.1f;
		return true;
	}

	return false;
}

bool SwayUpdate()
{
	float server_time = TICKS_TO_TIME(csgo->local->GetTickBase());
	static float next_break = 0.f;

	auto animstate = csgo->local->GetPlayerAnimState();
	if (!animstate)
		return false;

	if (animstate->m_velocity > 0.1f || fabs(animstate->flUpVelocity) > 100.f)
		next_break = server_time + 0.22f;

	if (next_break < server_time)
	{
		next_break = server_time + 0.22f;
		return true;
	}

	return false;
}

float CorrectedTickbase()
{
	CUserCmd* last_ucmd = nullptr;
	int corrected_tickbase = 0;

	corrected_tickbase = (!last_ucmd || last_ucmd->hasbeenpredicted) ? (float)csgo->local->GetTickBase() : corrected_tickbase++;
	last_ucmd = csgo->cmd;
	float corrected_curtime = corrected_tickbase * interfaces.global_vars->interval_per_tick;
	return corrected_curtime;
};

float GetCurtime() {
	if (!csgo->local)
		return 0;
	int g_tick = 0;
	CUserCmd* g_pLastCmd = nullptr;
	if (!g_pLastCmd || g_pLastCmd->hasbeenpredicted) {
		g_tick = csgo->local->GetTickBase();
	}
	else {
		++g_tick;
	}
	g_pLastCmd = csgo->cmd;
	float curtime = g_tick * interfaces.global_vars->interval_per_tick;
	return curtime;
}

float GetBackward(IBasePlayer* player) {
	return Math::CalculateAngle(csgo->local->GetOrigin(), player->GetOrigin()).y;
}

void CMAntiAim::Yaw()
{
	static bool _switch = false;
	static bool left = false, right = false, forward = false, back = true;
	IBasePlayer* best_ent = nullptr;
	float best_dist = FLT_MAX;
	bool inverted = vars.antiaim.inverter->active;

	switch (vars.antiaim.yaw)
	{
	case 0:
		break;
	case 1:
		csgo->cmd->viewangles.y -= 180.f;
		break;
	case 2:
		csgo->cmd->viewangles.y += 0.f;
		break;
	case 3:
		if (vars.antiaim.aa_override.enable)
		{
			if (GetAsyncKeyState(vars.antiaim.aa_override.left->key))
			{
				left = true;
				right = false;
				back = false;
				forward = false;
			}
			else if (GetAsyncKeyState(vars.antiaim.aa_override.right->key))
			{
				left = false;
				right = true;
				back = false;
				forward = false;
			}
			else if (GetAsyncKeyState(vars.antiaim.aa_override.back->key))
			{
				left = false;
				right = false;
				back = true;
				forward = false;
			}
			else if (GetAsyncKeyState(vars.antiaim.aa_override.forward->key))
			{
				left = false;
				right = false;
				back = false;
				forward = true;
			}

			if (left == true)
			{
				if (!inverted)
					csgo->cmd->viewangles.y += 90.f + vars.antiaim.body_lean / 10;
				else
					csgo->cmd->viewangles.y += 90.f + vars.antiaim.inverted_body_lean / 10;
			}
			if (right == true)
			{
				if (!inverted)
					csgo->cmd->viewangles.y -= 90.f + vars.antiaim.body_lean / 10;
				else
					csgo->cmd->viewangles.y -= 90.f + vars.antiaim.inverted_body_lean / 10;
			}
			if (back == true)
			{
				if (!inverted)
					csgo->cmd->viewangles.y -= 180.f + vars.antiaim.body_lean / 10;
				else
					csgo->cmd->viewangles.y -= 180.f + vars.antiaim.inverted_body_lean / 10;
			}
			if (forward == true)
			{
				if (!inverted)
					csgo->cmd->viewangles.y += 0.f + vars.antiaim.body_lean / 10;
				else
					csgo->cmd->viewangles.y += 0.f + vars.antiaim.inverted_body_lean / 10;
			}
		}
		break;
	case 4:
		for (int i = 1; i < 65; i++)
		{
			auto ent = interfaces.ent_list->GetClientEntity(i);
			if (!ent) {
				csgo->cmd->viewangles.y -= 180.f;
				continue;
			}
			if (!ent->isAlive() || ent == csgo->local || ent->GetTeam() == csgo->local->GetTeam())
			{
				csgo->cmd->viewangles.y -= 180.f;

				continue;
			}
			float dist = ent->GetOrigin().DistTo(csgo->local->GetOrigin());

			if (dist < best_dist)
			{
				best_ent = ent;
				best_dist = dist;
			}
		}

		if (best_ent && inverted)
			csgo->cmd->viewangles.y = Math::CalculateAngle(csgo->local->GetOrigin(), best_ent->GetOrigin()).y - 180.f + vars.antiaim.inverted_body_lean / 10;

		if (best_ent && !inverted)
			csgo->cmd->viewangles.y = Math::CalculateAngle(csgo->local->GetOrigin(), best_ent->GetOrigin()).y - 180.f + vars.antiaim.body_lean / 10;
		break;
	case 5:
		csgo->cmd->viewangles.y -= _switch ? vars.antiaim.micro_jitter_min : -vars.antiaim.micro_jitter_min;
		_switch = !_switch;
		break;
	case 6:
		csgo->cmd->viewangles.y += interfaces.global_vars->curtime * vars.antiaim.micro_jitter_max * 100;
		break;
	}
}

void CMAntiAim::predict_lby_update(float sampletime, CUserCmd* ucmd, bool& sendpacket)
{
	float server_time = TICKS_TO_TIME(csgo->local->GetTickBase());
	static float next_break = 0.f;

	auto animstate = csgo->local->GetPlayerAnimState();
	if (!animstate)
		csgo->InLbyUpdate = false;

	if (next_break < server_time)
	{
		next_break = server_time + 0.44f;
		csgo->InLbyUpdate = true;
	}

	csgo->InLbyUpdate = false;
}

void CMAntiAim::Desync(bool& send_packet)
{
	int side = csgo->SwitchAA ? 1 : -1;

	static bool sw = false, _switch = false;

	if (vars.antiaim.auto_inverter && csgo->desync_angle <= 2)
	{
		if (side == 1)
			side = -1.1;
		else if (side == -1)
			side = 1.1;
	}

	if (vars.ragebot.onshot_invert && csgo->cmd->buttons & IN_ATTACK)
	{
		if (side == 1)
			side = -1.1;
		else if (side == -1)
			side = 1.1;
	}

	if (vars.antiaim.freestand_fake) //this crashes your game sadly
	{
		IBasePlayer* plr;
		Vector src3D, dst3D, forward, right, up, src, dst;
		float back_two, right_two, left_two;
		trace_t tr;
		Ray_t ray, ray2, ray3, ray4, ray5;
		CTraceFilter filter;

		Math::AngleVectors(Vector(0, GetBackward(plr), 0), &forward, &right, &up);

		filter.pSkip = plr;
		src3D = plr->GetEyePosition();
		dst3D = src3D + (forward * 384); //Might want to experiment with other numbers, incase you don't know what the number does, its how far the trace will go. Lower = shorter.

		ray.Init(src3D, dst3D);
		interfaces.trace->TraceRay(ray, MASK_SHOT, &filter, &tr);
		back_two = (tr.endpos - tr.startpos).Length();

		ray2.Init(src3D + right * 35, dst3D + right * 35);
		interfaces.trace->TraceRay(ray2, MASK_SHOT, &filter, &tr);
		right_two = (tr.endpos - tr.startpos).Length();

		ray3.Init(src3D - right * 35, dst3D - right * 35);
		interfaces.trace->TraceRay(ray3, MASK_SHOT, &filter, &tr);
		left_two = (tr.endpos - tr.startpos).Length();

		if (left_two > right_two) {
			side = -1;
		}
		else if (right_two > left_two) {
			side = 1;
		}
	}

	if (vars.antiaim.lby_breaker)
	{
		bool lbyupdate = update();
		bool swayupdate = SwayUpdate();
		csgo->should_sidemove = false;
		static bool sway = false;
		static bool reverse = false;
		if (lbyupdate && csgo->local->GetVelocity().Length2D() < 6.0f)
		{
			csgo->should_sidemove = true;
			send_packet = false;

			switch (vars.antiaim.desync_type)
			{
			case 0:
				csgo->cmd->viewangles.y += 120.f * -side;
				break;
			case 1:
				if (swayupdate)
					csgo->cmd->viewangles.y += 120.f * side;
				break;
			case 2:
				if (sway)
					csgo->cmd->viewangles.y += 120.f * -side;
				break;
			}

			Math::NormalizeYaw(csgo->cmd->viewangles.y);
			sway = !sway;

			return;
		}
	}
	else
		csgo->should_sidemove = true;

	if (!send_packet)
	{
		if (vars.antiaim.desync_flicker->active)
			side = sw ? 1 : -1;

		if (vars.antiaim.inverter->active)
			csgo->cmd->viewangles.y += vars.antiaim.desync_amount_inv * side;
		else
			csgo->cmd->viewangles.y += vars.antiaim.desync_amount * side;

		if (vars.antiaim.inverter->active)
		{
			if (csgo->InLbyUpdate && csgo->local->GetVelocity().Length2D() < 6.0f)
			{
				csgo->cmd->viewangles.y += 120.f * -side;
				csgo->should_sidemove = false;
				send_packet = false;
				Math::NormalizeYaw(csgo->cmd->viewangles.y);
			}
		}
		else
		{
			if (csgo->InLbyUpdate && csgo->local->GetVelocity().Length2D() < 6.0f)
			{
				csgo->cmd->viewangles.y += 120.f * -side;
				csgo->should_sidemove = false;
				send_packet = false;
				Math::NormalizeYaw(csgo->cmd->viewangles.y);
			}
		}
	}

	if (vars.antiaim.fake_peek)
	{
		bool fakehead = fake_head();

		if (fakehead && csgo->local->GetVelocity().Length2D() < 6.0f)
		{
			if (vars.antiaim.fake_peek_inverter)
				csgo->cmd->viewangles.y += 90.f;
			else
				csgo->cmd->viewangles.y -= 90.f;
			csgo->send_packet = true;
		}
	}

	if (send_packet) {
		sw = !sw; _switch = !_switch;
	}
	else
		last_angle = csgo->cmd->viewangles.y;
}

inline float FastSqrt222(float x)
{
	unsigned int i = *(unsigned int*)&x;
	i += 127 << 23;
	i >>= 1;
	return *(float*)&i;
}

#define square( x ) ( x * x )

void MinWalk(CUserCmd* get_cmd, float get_speed)
{
	if (get_speed <= 0.f)
		return;

	float min_speed = (float)(FastSqrt222(square(get_cmd->forwardmove) + square(get_cmd->sidemove) + square(get_cmd->upmove)));
	if (min_speed <= 0.f)
		return;

	if (get_cmd->buttons & IN_DUCK)
		get_speed *= 2.94117647f;

	if (min_speed <= get_speed)
		return;

	float kys = get_speed / min_speed;

	get_cmd->forwardmove *= kys;
	get_cmd->sidemove *= kys;
	get_cmd->upmove *= kys;
}

void FastWalk(CUserCmd* get_cmd, float get_speed)
{
	auto weapon_handle = csgo->local->GetWeapon();

	if (!weapon_handle)
		return;

	float amount = 0.0034f * get_speed/*options.misc.slow_walk_amount*/; //max 100

	Vector velocity = csgo->local->GetVelocity();
	Vector direction;

	Math::VectorAngles(velocity, direction);

	float speed = velocity.Length2D();

	direction.y = get_cmd->viewangles.y - direction.y;

	Vector forward;

	Math::AngleVectors(direction, forward);

	Vector source = forward * -speed;

	if (speed >= (weapon_handle->GetCSWpnData(weapon_handle->GetItemDefinitionIndex())->m_flMaxSpeed * amount))
	{
		get_cmd->forwardmove = source.x;
		get_cmd->sidemove = source.y;
	}
}

void SlideTest(CUserCmd* get_cmd, float get_speed)
{
	auto weapon_handle = csgo->local->GetWeapon();

	if (!weapon_handle)
		return;

	float amount = 0.0034f * get_speed/*options.misc.slow_walk_amount*/; //max 100

	Vector velocity = csgo->local->GetVelocity();
	Vector direction;

	Math::VectorAngles(velocity, direction);

	float speed = velocity.Length2D();

	direction.y = get_cmd->viewangles.y - direction.y;

	Vector forward;

	Math::AngleVectors(direction, forward);

	Vector source = forward * -speed;

	if (speed * 2 >= (weapon_handle->GetCSWpnData(weapon_handle->GetItemDefinitionIndex())->m_flMaxSpeed * amount))
	{
		get_cmd->forwardmove = source.x;
		get_cmd->sidemove = source.y;
	}
}

void CMAntiAim::Run(bool& send_packet)
{
	int side = csgo->SwitchAA ? 1 : -1;
	static bool sw = false;

	switch (vars.antiaim.slowwalk_mode)
	{
	case 0:
		break;
	case 1:
	{
		if (vars.antiaim.slowwalk->active || csgo->should_stop_slide) {
			auto weapon_auto = csgo->local->GetWeapon()->GetItemDefinitionIndex() == WEAPON_G3SG1 && WEAPON_SCAR20;
			auto weapon_awp = csgo->local->GetWeapon()->GetItemDefinitionIndex() == WEAPON_AWP;
			auto weapon_ssg = csgo->local->GetWeapon()->GetItemDefinitionIndex() == WEAPON_SSG08;

			if (weapon_auto) {
				if (csgo->local->GetFlags() & FL_ONGROUND) {
					MinWalk(csgo->cmd, 35);
				}
			}
			if (weapon_awp) {
				if (csgo->local->GetFlags() & FL_ONGROUND) {
					MinWalk(csgo->cmd, 33);
				}
			}
			if (weapon_ssg) {
				if (csgo->local->GetFlags() & FL_ONGROUND) {
					MinWalk(csgo->cmd, 67);
				}
			}
			if (!weapon_awp && !weapon_auto && !weapon_ssg) {
				if (csgo->local->GetFlags() & FL_ONGROUND) {
					MinWalk(csgo->cmd, 33);
				}
			}
		}
	} break;
	case 2:
		if (vars.antiaim.slowwalk->active || csgo->should_stop_slide) {
			if (csgo->local->GetFlags() & FL_ONGROUND) {
				MinWalk(csgo->cmd, vars.antiaim.slowwalk_speed / 2);
			}
		}
		break;
	case 3:
		if (vars.antiaim.slowwalk->active || csgo->should_stop_slide) {
			if (csgo->local->GetFlags() & FL_ONGROUND) {
				static Vector oldOrigin = csgo->local->GetAbsOrigin();
				Vector velocity = (csgo->local->GetAbsOriginVec() - oldOrigin)
					* (1.f / interfaces.global_vars->interval_per_tick);
				oldOrigin = csgo->local->GetAbsOrigin();
				float speed = velocity.Length();

				if (speed > vars.antiaim.slowwalk_speed)
				{
					csgo->cmd->forwardmove = 0;
					csgo->cmd->sidemove = 0;
					csgo->send_packet = false;
				}
				else {
					csgo->send_packet = true;
				}
			}
		}
		break;
	case 4:
	{
		if (vars.antiaim.slowwalk->active || csgo->should_stop_slide) {
			if (csgo->local->GetFlags() & FL_ONGROUND) {
				SlideTest(csgo->cmd, 125);
			}
		}
	} break;
	case 5:
	{
		if (vars.antiaim.slowwalk->active || csgo->should_stop_slide) {
			if (csgo->local->GetFlags() & FL_ONGROUND) {
				SlideTest(csgo->cmd, 50);
			}
		}
	} break;
	}

	shouldAA = true;
	if (!vars.antiaim.enable) {
		shouldAA = false;
		return;
	}
	if (csgo->cmd->buttons & IN_USE)
	{
		shouldAA = false;
		return;
	}
	if (csgo->game_rules->IsFreezeTime()
		|| csgo->local->GetMoveType() == MOVETYPE_NOCLIP
		|| csgo->local->GetMoveType() == MOVETYPE_LADDER)
	{
		shouldAA = false;
		return;
	}
	bool shit = false;
	for (int i = 1; i < 65; i++)
	{
		auto ent = interfaces.ent_list->GetClientEntity(i);
		if (!ent)
			continue;
		if (
			!ent->isAlive()
			|| ent == csgo->local
			|| ent->GetTeam() == csgo->local->GetTeam()
			)
			continue;
		shit = true;
		break;

	}
	if (!shit)
	{
		if (csgo->ForceOffAA)
		{
			shouldAA = false;
			return;
		}
	}
	if (csgo->weapon->GetItemDefinitionIndex() == WEAPON_REVOLVER)
	{
		if (Ragebot::Get().shot)
		{
			if (!vars.ragebot.hideshots->active)
			{
				csgo->send_packet = true;
				shouldAA = false;
				return;
			}
			else if (vars.ragebot.hideshots->active)
			{
				csgo->send_packet = false; //psilent goes brrrrr
				shouldAA = false;
				return;
			}
			if (!vars.antiaim.fakeduck->active)
			{
				csgo->send_packet = true;
				shouldAA = false;
				return;
			}
			else if (vars.antiaim.fakeduck->active)
			{
				csgo->send_packet = false; //dont stand up please
				shouldAA = false;
				return;
			}
		}
	}
	else
	{
		if (F::Shooting() && vars.antiaim.antiaim_onshot || csgo->TickShifted && vars.antiaim.antiaim_onshot || (CanDT() && csgo->cmd->buttons & IN_ATTACK) && vars.antiaim.antiaim_onshot)
		{
			csgo->send_packet = false;
			shouldAA = true;
		}
		if (F::Shooting() || csgo->TickShifted || (CanDT() && csgo->cmd->buttons & IN_ATTACK))
		{
			if (!vars.ragebot.hideshots->active)
			{
				csgo->send_packet = true;
				shouldAA = false;
				return;
			}
			else if (vars.ragebot.hideshots->active)
			{
				csgo->send_packet = false;
				shouldAA = false;
				return;
			}
			if (!vars.antiaim.fakeduck->active)
			{
				csgo->send_packet = true;
				shouldAA = false;
				return;
			}
			else if (vars.antiaim.fakeduck->active)
			{
				csgo->send_packet = false; //dont stand up please
				shouldAA = false;
				return;
			}
		}
		if (csgo->weapon->IsKnife() && csgo->cmd->buttons & IN_ATTACK && Ragebot::Get().IsAbleToShoot() || csgo->weapon->IsKnife() && csgo->cmd->buttons & IN_ATTACK2 && Ragebot::Get().IsAbleToShoot())
		{
			if (!vars.ragebot.hideshots->active)
			{
				csgo->send_packet = true;
				shouldAA = false;
				return;
			}
			else if (vars.ragebot.hideshots->active)
			{
				csgo->send_packet = false; //bro this is premium psilent
				shouldAA = false;
				return;
			}
			if (!vars.antiaim.fakeduck->active)
			{
				csgo->send_packet = true;
				shouldAA = false;
				return;
			}
			else if (vars.antiaim.fakeduck->active)
			{
				csgo->send_packet = false; //dont stand up please
				shouldAA = false;
				return;
			}
		}
	}
	if (shouldAA)
	{
		Pitch();
		Yaw();
		Desync(send_packet);
	}
	if (send_packet)
		sw = !sw;
}