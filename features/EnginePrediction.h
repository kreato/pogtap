#pragma once
#include "../hooks/Hooks.h"

struct backup
{
    int flags = 0;
    Vector velocity = Vector(0, 0, 0);
};

class CEnginePrediction : public Singleton<CEnginePrediction>
{
public:
    void Start(CUserCmd* cmd, IBasePlayer* local);
    void Finish(IBasePlayer* local);

    int GetTickbase(CUserCmd* pCmd, IBasePlayer* pLocal);

    backup backup_data;
private:
    CMoveData data;
    CUserCmd* last_cmd{};
    int32_t tick_base{};
    struct {
        float curtime, frametime;
        int tickcount, tickbase;
        bool m_in_prediction, m_first_time_predicted;
    }old_vars;
    int* prediction_random_seed = nullptr;
    int* prediction_player = nullptr;
};