#include "../hooks/Hooks.h"
#include "Resolver.h"
#include "RageBacktracking.h"
#include "Ragebot.h"
#include "AnimationFix.h"


std::string ResolverMode[65];
int last_ticks[65];
bool hasDesyncHadTimeToDesync = false;
bool hasDesyncHadTimeToLBY = false;
#define RANDOME_FLOAT(x) ( static_cast<float>(static_cast<float>(rand()/ static_cast<float>(RAND_MAX/ ( x ) ))) )
int IBasePlayer::GetChokedPackets() {
	auto ticks = TIME_TO_TICKS(GetSimulationTime() - GetOldSimulationTime());
	if (ticks == 0 && last_ticks[GetIndex()] > 0) {
		return last_ticks[GetIndex()] - 1;
	}
	else {
		last_ticks[GetIndex()] = ticks;
		return ticks;
	}
}
float normalize_pitch(float pitch)
{
	while (pitch > 89.0f)
		pitch -= 180.0f;

	while (pitch < -89.0f)
		pitch += 180.0f;

	return pitch;
}
float CResolver::GetAngle(IBasePlayer* player) {
	return Math::NormalizeYaw(player->GetEyeAngles().y);
}

float CResolver::GetForwardYaw(IBasePlayer* player) {
	return Math::NormalizeYaw(GetBackwardYaw(player) - 180.f);
}

float CResolver::GetBackwardYaw(IBasePlayer* player) {
	return Math::CalculateAngle(csgo->local->GetOrigin(), player->GetOrigin()).y;
}

float CResolver::GetLeftYaw(IBasePlayer* player) {
	return Math::NormalizeYaw(Math::CalculateAngle(csgo->local->GetOrigin(), player->GetOrigin()).y - 90.f);
}

float CResolver::GetRightYaw(IBasePlayer* player) {
	return Math::NormalizeYaw(Math::CalculateAngle(csgo->local->GetOrigin(), player->GetOrigin()).y + 90.f);
}
bool CResolver::TargetJitter(IBasePlayer* player, bool v2) {
	float yaw = v2 ? GetRightYaw(player) : GetLeftYaw(player);
	return fabsf(GetAngle(player) - Math::NormalizeYaw(yaw + 90.f))
		>= fabsf(GetAngle(player) - Math::NormalizeYaw(yaw - 90.f));
}
bool CResolver::TargetSide(IBasePlayer* player) {
	float yaw = Math::NormalizeYaw(GetBackwardYaw(player));
	float angle = GetAngle(player);
	return fabsf(angle - Math::NormalizeYaw(yaw + 90.f))
		>= fabsf(angle - Math::NormalizeYaw(yaw - 90.f));
}

void CResolver::DetectSide(IBasePlayer* player, int *side)
{
	Vector src3D, dst3D, forward, right, up, src, dst;
	float back_two, right_two, left_two;
	trace_t tr;
	Ray_t ray, ray2, ray3, ray4, ray5;
	CTraceFilter filter;

	Math::AngleVectors(Vector(0, GetBackwardYaw(player), 0), &forward, &right, &up);

	filter.pSkip = player;
	src3D = player->GetEyePosition();
	dst3D = src3D + (forward * 384); //Might want to experiment with other numbers, incase you don't know what the number does, its how far the trace will go. Lower = shorter.

	ray.Init(src3D, dst3D);
	interfaces.trace->TraceRay(ray, MASK_SHOT, &filter, &tr);
	back_two = (tr.endpos - tr.startpos).Length();

	ray2.Init(src3D + right * 35, dst3D + right * 35);
	interfaces.trace->TraceRay(ray2, MASK_SHOT, &filter, &tr);
	right_two = (tr.endpos - tr.startpos).Length();

	ray3.Init(src3D - right * 35, dst3D - right * 35);
	interfaces.trace->TraceRay(ray3, MASK_SHOT, &filter, &tr);
	left_two = (tr.endpos - tr.startpos).Length();

	if (left_two > right_two) {
		*side = -1;
		//Body should be right
	}
	else if (right_two > left_two) {
		*side = 1;
	}
	else
		*side = 0;
}

bool CResolver::DoesHaveJitter(IBasePlayer* player, int *new_side) {
	static float LastAngle[64];
	static int LastBrute[64];
	static bool Switch[64];
	static float LastUpdateTime[64];

	int i = player->GetIndex();

	float CurrentAngle = player->GetEyeAngles().y;
	if (!Math::IsNearEqual(CurrentAngle, LastAngle[i], 50.f)) {
		Switch[i] = !Switch[i];
		LastAngle[i] = CurrentAngle;
		*new_side = Switch[i] ? 1 : -1;
		LastBrute[i] = *new_side;
		LastUpdateTime[i] = interfaces.global_vars->curtime;
		return true;
	}
	else {
		if (fabsf(LastUpdateTime[i] - interfaces.global_vars->curtime >= TICKS_TO_TIME(17))
			|| player->GetSimulationTime() != player->GetOldSimulationTime()) {
			LastAngle[i] = CurrentAngle;
		}
		*new_side = LastBrute[i];
	}
	return false;
}


void CResolver::StoreAntifreestand()
{
	if (!csgo->local->isAlive())
		return;

	if (!csgo->weapon->IsGun())
		return;


	for (int i = 1; i < interfaces.engine->GetMaxClients(); ++i)
	{
		auto player = interfaces.ent_list->GetClientEntity(i);

		if (!player || !player->isAlive() || player->IsDormant() || player->GetTeam() == csgo->local->GetTeam())
			continue;

		bool Autowalled = false, HitSide1 = false, HitSide2 = false;
		auto idx = player->GetIndex();
		float angToLocal = Math::CalculateAngle(csgo->local->GetOrigin(), player->GetOrigin()).y;
		Vector ViewPoint = csgo->local->GetOrigin() + Vector(0, 0, 90);
		Vector2D Side1 = { (45 * sin(DEG2RAD(angToLocal))),(45 * cos(DEG2RAD(angToLocal))) };
		Vector2D Side2 = { (45 * sin(DEG2RAD(angToLocal + 180))) ,(45 * cos(DEG2RAD(angToLocal + 180))) };

		Vector2D Side3 = { (50 * sin(DEG2RAD(angToLocal))),(50 * cos(DEG2RAD(angToLocal))) };
		Vector2D Side4 = { (50 * sin(DEG2RAD(angToLocal + 180))) ,(50 * cos(DEG2RAD(angToLocal + 180))) };

		Vector Origin = player->GetOrigin();

		Vector2D OriginLeftRight[] = { Vector2D(Side1.x, Side1.y), Vector2D(Side2.x, Side2.y) };

		Vector2D OriginLeftRightLocal[] = { Vector2D(Side3.x, Side3.y), Vector2D(Side4.x, Side4.y) };

		for (int side = 0; side < 2; side++)
		{
			Vector OriginAutowall = { Origin.x + OriginLeftRight[side].x,  Origin.y - OriginLeftRight[side].y , Origin.z + 90 };
			Vector ViewPointAutowall = { ViewPoint.x + OriginLeftRightLocal[side].x,  ViewPoint.y - OriginLeftRightLocal[side].y , ViewPoint.z };

			if (g_AutoWall.CanHitFloatingPoint(OriginAutowall, ViewPoint))
			{
				if (side == 0)
				{
					HitSide1 = true;
					FreestandSide[idx] = -1;
				}
				else if (side == 1)
				{
					HitSide2 = true;
					FreestandSide[idx] = 1;
				}

				Autowalled = true;
			}
			else
			{
				for (int sidealternative = 0; sidealternative < 2; sidealternative++)
				{
					Vector ViewPointAutowallalternative = { Origin.x + OriginLeftRight[sidealternative].x,  Origin.y - OriginLeftRight[sidealternative].y , Origin.z + 90 };

					if (g_AutoWall.CanHitFloatingPoint(ViewPointAutowallalternative, ViewPointAutowall))
					{
						if (sidealternative == 0)
						{
							HitSide1 = true;
							FreestandSide[idx] = -1;
							//FreestandAngle[pPlayerEntity->EntIndex()] = 90;
						}
						else if (sidealternative == 1)
						{
							HitSide2 = true;
							FreestandSide[idx] = 1;
							//FreestandAngle[pPlayerEntity->EntIndex()] = -90;
						}

						Autowalled = true;
					}
				}
			}
		}
	}
}
bool CheckDesync(CCSGOPlayerAnimState* Animstate, IBasePlayer* player) {
	float Velocity = player->GetVelocity().Length2D();
	bool PresumedNoDesync = false;
	if ((Velocity < 140) && (Animstate->m_flTimeSinceStartedMoving <= 1.2f)) {
		if (Animstate->m_flTimeSinceStartedMoving <= 1.2f) {
			if (Animstate->m_flTimeSinceStartedMoving >= .22f) {
				hasDesyncHadTimeToLBY = true;
				if (Animstate->m_flTimeSinceStoppedMoving >= .8f) {
					hasDesyncHadTimeToDesync = true;
				}
			}
		}
		else if (Animstate->m_flTimeSinceStartedMoving > 0) {
			if (Velocity > 100) {
				hasDesyncHadTimeToDesync = true;
			}
		}
	}
	else {

		PresumedNoDesync = true;
	}
	return PresumedNoDesync;
}


void CResolver::Do(IBasePlayer* player) {
	auto animstate = player->GetPlayerAnimState();
	if (!animstate)
		return;
	if (!vars.ragebot.resolver)
		return;
	if (!csgo->local->isAlive())
		return;
	if (player->GetChokedPackets() <= 0)
		return;

	int idx = player->GetIndex();

	if (player->GetPlayerInfo().fakeplayer)
		return;

	int wap;
	int previous_ticks[128];
	auto ticks = TIME_TO_TICKS(player->GetSimulationTime() - player->GetOldSimulationTime());
	auto valid_lby = true;
	auto eyeangle = player->GetEyeAngles();
	if (csgo->missedshots[idx] > 4)
		csgo->missedshots[idx] = 0;
	if ((eyeangle.x < 65.f || eyeangle.x > 89.f))
	{
			float trueDelta = Math::NormalizeYaw(animstate->m_flGoalFeetYaw - eyeangle.y);
			if (player->GetVelocity().Length() > 0.1f || fabs(animstate->flUpVelocity) > 100.f)
				valid_lby = animstate->m_flTimeSinceStartedMoving < 0.22f;
			switch (csgo->missedshots[idx])
			{
			case 0:
				if (valid_lby)
					animstate->m_flGoalFeetYaw+= 115;
				else
					eyeangle.y += 58;
				break;
			case 1:
				if (valid_lby)
					animstate->m_flGoalFeetYaw -= 115;
				else
					eyeangle.y -= 58;
				break;
			case 2:
				eyeangle.y = trueDelta == 0 ? eyeangle.y - 30.f : eyeangle.y + trueDelta;
				break;
			case 3:
				eyeangle.y += trueDelta;
				break;
			case 4:
				eyeangle.y = trueDelta == 0 ? eyeangle.y - 37.f : eyeangle.y + trueDelta;
				break;
			default:
				break;
		}
	}
	else {
		float trueDelta = Math::NormalizeYaw(animstate->m_flGoalFeetYaw - eyeangle.y);
		float angle = GetAngle(player);
		float fuck = 90;
		eyeangle.x = normalize_pitch(eyeangle.x);
		float delta = Math::NormalizeYaw(eyeangle.y - animstate->m_flGoalFeetYaw);
		float initialyaw = 0.f;
		bool desynctime = CheckDesync(animstate, player);
		if (player->GetVelocity().Length() > 0.1f || fabs(animstate->flUpVelocity) > 100.f)
			valid_lby = animstate->m_flTimeSinceStartedMoving < 0.22f;
		if (hasDesyncHadTimeToDesync || hasDesyncHadTimeToLBY) {
			if (fabs(delta) > 30.0f && valid_lby)
			{
				if (delta > 30.0f)
				{
					initialyaw = Math::NormalizeYaw(eyeangle.y + 60.0f);
				}
				else if (delta < -30.0f)
				{
					initialyaw = Math::NormalizeYaw(eyeangle.y - 60.0f);
				}
			}
			else {
				if (delta > 0.0f)
					initialyaw = Math::NormalizeYaw(eyeangle.y + 30.0f);
				else
					initialyaw = Math::NormalizeYaw(eyeangle.y - 30.0f);


			}
		}
		else if (desynctime && !(hasDesyncHadTimeToDesync || hasDesyncHadTimeToLBY)) {
			initialyaw = 0;
		}
		switch (csgo->missedshots[idx])
		{
		case 0:
			animstate->m_flGoalFeetYaw = Math::NormalizeYaw(initialyaw);
			break;
		case 1:
			animstate->m_flGoalFeetYaw = Math::NormalizeYaw(-initialyaw);
			break;
		case 2:
			//Nope. not pasting that shit in
			break;
		case 3:
			eyeangle.y = trueDelta <= 0 ? eyeangle.y - 30.f : eyeangle.y + 30.f; //meme steps
			break;
		case 4:
			eyeangle.y += trueDelta <= 0 ? eyeangle.y - RANDOME_FLOAT(35.f) : eyeangle.y + RANDOME_FLOAT(35.f); //Extreme memeage step
			break;
		default:
			break;
		}

	}
	}



